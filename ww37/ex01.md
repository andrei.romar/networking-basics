MAC addresses:
https://whatismyipaddress.com/mac-address , https://macvendors.com/what-is-a-mac-address and https://en.wikipedia.org/wiki/MAC_address 
The first sites contain some trials and explanations over the IT topics, so information written there can be reliable. Wiki is never reliable, but the sources in the bottom are quite trustworthy. Now because I have a primary source(first 2) and secondary(wiki) I can rely on this information.
These sources provide extra information over the MAC addresses, like the "base" addresses for some known manufacturers(f.e. MAC addresses starting with 00-14-22 belong to Dell e.t.c.)

IP address
https://whatismyipaddress.com/ip-address , https://www.whatismyip.com/what-is-an-ip-address/ and https://en.wikipedia.org/wiki/IP_address
Same as previous time. 2 different sites with similar information about the IT topic and wiki as secondary source for some extra information and sources
These sources also provide interesting facts, like brief explanation of what IPv4 and IPv6 are.

Network(+physical vs logical topology):
https://www.computerhope.com/jargon/n/network.htm , https://fcit.usf.edu/network/chap1/chap1.htm and https://www.pcmag.com/encyclopedia/term/logical-vs-physical
This time without google, since it will contaion too much not relevant information for now, which will be hard to understand.
These sites provides just a bit more of basic information, which are quite good for creating a stronger base before starting to learn deeper things about the network. Also by using information from the 3rd and 1st site it becomes much easier to imagine how physical topology looks like.

Subnet and network masks
https://www.cloudflare.com/learning/network-layer/what-is-a-subnet/ and https://www.calculator.net/ip-subnet-calculator.html
Already now many topics start to come together and appear as a whole. To understand that an understanding of the IP addresses and network are required.
These sites explain what is subnet and what for subnet masks are required. 