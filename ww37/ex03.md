1.The text is below
2. a) What virtual machines are used for
https://www.howtogeek.com/196060/beginner-geek-how-to-create-and-use-virtual-machines/#:~:text=Virtual%20machines%20allow%20you%20to,in%20a%20safe%2C%20sandboxed%20environment.
b)A bit more of what exactly is virtualization
https://www.redhat.com/en/topics/virtualization/what-is-virtualization
c)Limitations of virtualization
https://subscription.packtpub.com/book/virtualization_and_cloud/9781785887932/1/ch01lvl1sec9/limitations-of-virtualization
d)Types of virtualization
https://searchservervirtualization.techtarget.com/definition/virtualization#:~:text=Storage%20virtualization%20is%20commonly%20used,systems%20%2D%2D%20from%20server%20users.
e)Is virtualization secure?
https://www.juniper.net/us/en/products-services/what-is/security-virtualization/#:~:text=Virtualization%20is%20the%20process%20of,infrastructure%20is%20secure%20and%20protected.&text=File%20sharing%20between%20hosts%20and%20guests%20is%20not%20secure.



My words about Virtualization based on Feynman technique
Virtualization feels similar to the multitasking, but after some point it starts to feel as a system to complete multiple tasks. This feels to be really useful due to the some systematic limitations, but dividing strengths is a bit risky and not always worthy game to play.
Comparing to humans it can be an example of doing multiple things at once. It is possible to listen to the music, cook, have a conversation and study at the same time until you're able to manage all processes at once. But even if you're able to do that it's not comfortable to have such a big spread, so in the case if you'd be able to make your clone to deal with some of the tasks it would run much easier. The question that comes is how many cloes you'd be able to create, so thye would be able to manage the tasks you need and how much power/energy each of them needs. This example was in the theoretic world, where fantasies can become true, but in real life it's impossible to clone a human, so machines win in this area.
Now talking seriously about the virtualization and its possibilities. Each virtual machine created by host is limited in memory, power and storage and their sum will never be able to exceed the host parameters. It allows to have multiple OS on same pc, work from multiple accounts or hold separately some data/servers/whaterver. The easiest variant to understand it is to compare it to have a multiple tabs in the browser. You can create as many tabs on a single browser as you want until you will run out of resources. Sometimes you need to use multiple accounts or work at the same time with many tabs. Due to that amount of tabs can grow incredibly and it will become uncomfortable to work with. Then you can use some other browsers to spread the data between them to make sorting of your tasks and working with multiple accounts on same platform much easier(almost like a dividng files by folders to make it easier to find). The problem that comes now is the limitation of the resources. Amount of memory/storage/power required will not become more,  since the total characterstics stay the same, so you will not be able to do in each browser as much as you did when there was only 1 of them.
Based on these examples I wrote, I can assume, that virtual machines are really similar to the basic working with multiple tasks. Just on a bit larger scale for more specific needs(f.e. IT rpogramming e.t.c.) on a strong machines.