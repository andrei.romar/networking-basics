Part 1:

Frame: sniffed information about the package
Ethernet II, Src: sniffed information about the MAC addresses of the requester and replier (00:0c:29:96:72:bc and 00:50:56:e6:8f:e9)
IPv4: sniffed IP addresses of the requster and replier in the IPv4 format (192.168.145.128 and 8.8.8.8)
Internet Control Message Protocol: sniffed package data


Part 2:
All the same, except replier's IP address, which is now 1.1.1.1 and ICMP, since another data package was sent. Same MAC address means that both IP addresses belong to the same VM.