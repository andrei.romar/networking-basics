https://www.youtube.com/watch?v=b_XQ7Xg1Jek

1st way is placed on the physical layer and is called a tap method, which directly adds some sniffing devices in the routing.

2nd way is the mirror port, which collects the data and normally is in transport layer.

3rd way is the interface. This way even user can check his data using some programms, but also browsers e.t.c can sniff the data. Here comes security problem, since if other 2 collect encrypted data, this one can collect decrypted.